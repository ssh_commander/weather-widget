const BASE_API_URL = "http://api.openweathermap.org/data/2.5/";
const KEY = "0bd3f0724af19f30b047bf8372bcdc98";
const DEFAULT_CITIES = [524901, 588409, 2643743, 2950159];

export {
  KEY,
  BASE_API_URL,
  DEFAULT_CITIES
};